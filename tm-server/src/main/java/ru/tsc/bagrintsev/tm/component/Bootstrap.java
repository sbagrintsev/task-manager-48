package ru.tsc.bagrintsev.tm.component;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.*;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.api.sevice.dto.*;
import ru.tsc.bagrintsev.tm.endpoint.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.DomainService;
import ru.tsc.bagrintsev.tm.service.LoggerService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.dto.*;
import ru.tsc.bagrintsev.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(projectService, taskService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(projectService, taskService, propertyService, connectionService);

    @NotNull
    private final IAuthServiceDTO authService = new AuthServiceDTO(userService, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        boolean exists;
        try {
            userService.findByLogin("admin");
            exists = true;
        } catch (AbstractException e) {
            exists = false;
        }
        if (!exists) {
            try {
                userService.create("admin", "admin");
                userService.setRole("admin", Role.ADMIN);
            } catch (GeneralSecurityException | AbstractException e) {
                System.err.println("User initialization error...");
                loggerService.error(e);
            }
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?wsdl", host, port, name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() throws DomainNotFoundException, IOException, ClassNotFoundException {
        initPID();
        initUsers();
        loggerService.info("*** Task Manager Server Started ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
//        backup.start();
    }

    private void shutdown() {
        loggerService.info("*** Task Manager Server Stopped ***");
        connectionService.close();
//        backup.stop();
    }

}
