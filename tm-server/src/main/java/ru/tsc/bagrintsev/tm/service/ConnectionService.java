package ru.tsc.bagrintsev.tm.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.dto.model.SessionDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    private final String CACHE_PROVIDER_CONFIG = "hibernate.cache.provider_configuration_file_resource_path";

    private final String USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = getEntityManagerFactory();
    }

    public void close() {
        entityManagerFactory.close();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, Object> settings = new HashMap<>();
        settings.put(AvailableSettings.DRIVER, propertyService.getDatabaseDriver());
        settings.put(AvailableSettings.URL, propertyService.getDatabaseUrl());
        settings.put(AvailableSettings.USER, propertyService.getDatabaseUserName());
        settings.put(AvailableSettings.PASS, propertyService.getDatabaseUserPassword());
        settings.put(AvailableSettings.DIALECT, propertyService.getDatabaseSqlDialect());
        settings.put(AvailableSettings.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        settings.put(AvailableSettings.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(AvailableSettings.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        settings.put(AvailableSettings.USE_SQL_COMMENTS, propertyService.getDatabaseUseSqlComments());
        settings.put(AvailableSettings.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLevelCache());
        settings.put(CACHE_PROVIDER_CONFIG, propertyService.getDatabaseCacheProvConfigFileResPath());
        settings.put(AvailableSettings.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        settings.put(AvailableSettings.USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        settings.put(AvailableSettings.USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        settings.put(USE_LITE_MEMBER, propertyService.getDatabaseCacheHazelcastUseLiteMember());
        settings.put(AvailableSettings.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(SessionDTO.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
