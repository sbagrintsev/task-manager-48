package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.hibernate.jpa.HibernateHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.util.List;

public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(
            @NotNull final EntityManager entityManager
    ) {
        super(UserDTO.class, entityManager);
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        @NotNull final List<UserDTO> result = entityManager
                .createQuery(jpql, clazz)
                .setHint(HibernateHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public @Nullable UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        @NotNull final List<UserDTO> result = entityManager
                .createQuery(jpql, clazz)
                .setHint(HibernateHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public boolean isEmailExists(@NotNull String email) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExists(@NotNull String login) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.remove(findByLogin(login));
    }

    @Override
    public void setParameter(@NotNull final UserDTO user) {
        entityManager.merge(user);
    }

    @Override
    public void setRole(
            @NotNull final String login,
            @NotNull final Role role
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.role = :role " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("role", role)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.passwordHash = :password, " +
                        "m.passwordSalt = :salt " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("login", login)
                .setParameter("password", password)
                .setParameter("salt", salt)
                .executeUpdate();
    }

}
