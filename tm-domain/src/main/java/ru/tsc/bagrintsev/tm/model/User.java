package ru.tsc.bagrintsev.tm.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "m_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractModel {

    @NotNull
    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @NotNull
    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column(name = "password_salt", nullable = false)
    private byte @NotNull [] passwordSalt;

    @Nullable
    @Column(name = "email", unique = true)
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.REGULAR;

    @NotNull
    @Column(name = "is_locked", nullable = false)
    private Boolean locked = false;

    @NotNull
    @Transient
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @Transient
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Transient
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            final byte @NotNull [] passwordSalt
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.passwordSalt = passwordSalt;
    }

}
